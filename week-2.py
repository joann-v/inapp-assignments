import numpy as np
import random as rand
 
# function to display board
def display_board(board):
    for i in board:
        print('   |   |')
        print(' ' + i[0] + ' | ' + i[1] + ' | ' + i[2])
        print('   |   |')
        print('-----------')

# function for player to choose marker
def player_input():
    marker = ''
    while not (marker == 'X' or marker == 'O'):
        marker = input('Player: Do you want to be X or O? ').upper()
    if marker == 'X':
        return ('X', 'O')
    else:
        return ('O', 'X')

# function to place marker on the board at the requested location
def place_marker(board, marker, position):
    global theBoard
    flatboard = np.ravel(theBoard)
    flatboard[position] = marker
    theBoard = np.array(flatboard).reshape(3,3)
    
# function to check if the winning condition is satisfied
def win_check(board, marker):
    rows_win = cols_win = False
    for i in range(0, 3):
        if (board[i, :] == marker).all() or (board[:, i] == marker).all():
            rows_win = (board[i, :] == marker).all()
            cols_win = (board[:, i] == marker).all()
    diag1 = (np.diag(board) == marker).all()
    diag2 = (np.diag(np.fliplr(board)) == marker).all()
    if rows_win or cols_win or diag1 or diag2:
        return True

# function to choose if Player 1 or Player 2's chance
def choose_first():
    n = rand.randint(0, 1)
    if n == 0:
        return 'Computer'
    else:
        return 'Player'

# function to check if the requested position is available to place marker
def space_check(board, position):
    flatboard = np.ravel(board)
    return flatboard[position] == ' '

# function to check if there is any space in board that is available
def full_board_check(board):
    for i in range(0,9):
        if space_check(board, i):
            return False
    return True

# function that asks for a player's next position (as a number 0-8) and then uses the function from step 6 to check if its a free position.
def player_choice(board):
    position = -1
    while position not in [0,1,2,3,4,5,6,7,8] or not space_check(board, position):
        position = int(input('Choose your next position: (0-8) ')) 
    return position

# function for computer to select random position
def computer_choice(board):
    position = -1
    while position not in [0,1,2,3,4,5,6,7,8] or not space_check(board, position):
        position = rand.randint(0, 8)
    return position

history = dict()
#function to create a game dictionary in history
def create_game(gno):
    history[gno] = {} 

#function to store history of the games & rounds
def store_history(gno, rno, board):
    flatboard = np.ravel(board) 
    history[gno][rno] = flatboard[:]
    

#function to store winner details
def store_winner(gno, winner):
    history[gno]["winner"] = winner

# function to print history
def print_history(history, index):
    for i in range(1, 10):
        if i in history[index].keys():
            print(f"Round-{i}\n")
            b = history[index][i]
            display_board(b.reshape(3, 3))
    print(f"{history[index]['winner']} won Game-{index}")

# MAIN PROGRAM
game_number = 1
print('Welcome to Tic Tac Toe!')
no_of_games = int(input("How many games do you want to play? "))

while game_number <= no_of_games:
    # Reset the board
    rows, cols = (3, 3)
    theBoard = [[" "]*cols]*rows
    print(f"\nGAME {game_number}")
    create_game(game_number)
    player_marker, computer_marker = player_input()
    turn = choose_first()
    print(turn + ' will go first.')
    play_game = input('Are you ready to play? Enter Yes or No.')
    
    if play_game.lower()[0] == 'y':
        game_on = True
    else:
        game_on = False

    round_number = 0
    while game_on:
        if turn == 'Player':
            round_number += 1
            # Player's turn.
            print(f"\nPlayer's turn- {round_number}")

            position = player_choice(theBoard)
            place_marker(theBoard, player_marker, position)
            display_board(theBoard)
            store_history(game_number, round_number ,theBoard)
            
            if win_check(theBoard, player_marker):
                print('Congratulations! You have won the game!')
                store_winner(game_number, "Player")
                display_board(theBoard)
                game_on = False
            else:
                if full_board_check(theBoard):
                    print('The game is a draw!')
                    store_winner(game_number, "Both")
                    display_board(theBoard)
                    break
                else:
                    turn = 'Computer'  
            
        else:
            round_number += 1
            # Computer's turn.
            print(f"\nComputer's turn- {round_number}")

            position = computer_choice(theBoard)
            place_marker(theBoard, computer_marker, position)
            display_board(theBoard)
            store_history(game_number, round_number ,theBoard)
            
            if win_check(theBoard, computer_marker):
                print('Computer has won!')
                store_winner(game_number, "Computer")
                display_board(theBoard)
                game_on = False
            else:
                if full_board_check(theBoard):
                    print('The game is a draw!')
                    store_winner(game_number, "Both")
                    display_board(theBoard)
                    break
                else:
                    turn = 'Player'
        
    game_number += 1
    
while input("\nDo you want to see game history?(y/n)").lower()[0] == 'y':
    ind= int(input(f"Enter the game for which you need the information(1-{no_of_games}): "))
    print_history(history, ind)
