import sqlite3

conn = sqlite3.connect(':memory:')
conn.row_factory = sqlite3.Row
c = conn.cursor()

def create_employee():
    c.execute('''CREATE TABLE employee
               (e_id text primary key, 
                e_name text, 
                salary int,
                d_id text,
                FOREIGN KEY(d_id) REFERENCES department(d_id))''')

def create_column_city():
    c.execute('''ALTER TABLE employee ADD city text''')

emp_list = [
    ('101', 'David', 40000, '1', 'Delhi'),
    ('102', 'Michael', 20000, '1', 'Thiruvananthapuram'),
    ('103', 'Susan', 25000, '2', 'Kolkata'),
    ('104', 'Robert', 28000, '2', 'Kochi'),
    ('105', 'Linda', 42000, '3', 'Kolkata')]

def insert_emprecords():
    c.executemany("INSERT INTO employee VALUES (?, ?, ?, ?, ?)", emp_list)

def print_all_emprecords():
    print("\nEMPLOYEES")
    c.execute("SELECT * FROM employee")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Employee ID- {record['e_id']}, 
                  Employee name- {record['e_name']}, 
                  Salary- {record['salary']}""")

def printl_emp(letter):
    c.execute(f"SELECT * FROM employee WHERE e_name LIKE '{letter}%'")
    results = c.fetchall()
    for record in results:
        print(f"{record['e_id']}, {record['e_name']}, {record['salary']}")
    if len(results) == 0:
        print("No entries!")

def printid_emp(id):
    c.execute("SELECT * FROM employee WHERE e_id = :eid", {'eid' : id})
    results = c.fetchall()
    for record in results:
        print(f"{record['e_id']}, {record['e_name']}, {record['salary']}")
    if len(results) == 0:
        print("No entries!")

def change_name(id):
    new_name = input("Enter new name of the employee: ")
    c.execute("""UPDATE employee
                SET e_name = :name
                WHERE e_id = :eid""", {'name' : new_name, 'eid' : id})

def create_dept():
    c.execute('''CREATE TABLE department
               (d_id text primary key, 
                d_name text)''')

dept_list = [
    ('1', 'HR'),
    ('2', 'Development'),
    ('3', 'Management'),
    ('4', 'Marketing')]

def insert_deptrecords():
    c.executemany("INSERT INTO department VALUES (?, ?)", dept_list)

def print_all_deptrecords():
    print("\nDEPARTMENTS")
    c.execute("SELECT * FROM department")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Department ID- {record['d_id']}, 
                  Department name- {record['d_name']}""")

def set_fk():
    c.execute("ALTER TABLE employee ADD CONSTRAINT fk_child_parent FOREIGN KEY (d_id) REFERENCES department(d_id)")

def print_allin_dept(id):
    c.execute("SELECT * FROM employee WHERE d_id = :did", {'did' : id})
    results = c.fetchall()
    for record in results:
        print(f"{record['e_id']}, {record['e_name']}, {record['salary']}")
    if len(results) == 0:
        print("No entries!")