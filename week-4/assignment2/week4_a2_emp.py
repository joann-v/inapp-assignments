import week4_a2_sql as sql

#QUESTION 2
# a. Create one more Table called Departments with two columns Department_id and Department_name.
sql.create_dept()
# b. Insert 5 records into this table
sql.insert_deptrecords()
sql.print_all_deptrecords()

# QUESTION 1
# a. Create a Table Employee which will have the four columns, i.e., Name, ID, salary, and Department_id.
sql.create_employee()
# b. Add a new column ‘City’ to the Table Employee.
sql.create_column_city()
# c. Insert 5 records into this table.
sql.insert_emprecords()
# d. Read the Name, ID, and Salary from the Employee table and print it.
sql.print_all_emprecords()
# e. Print the details of employees whose names start with ‘j’ (or any letter input by the user)
letter = input("Enter a letter: ")
sql.printl_emp(letter.capitalize())
# f. Print the details of employees with ID’s inputted by the user.
eid = input("Enter an id: ")
sql.printid_emp(eid)
# g. Change the name of the employee whose ID is input by the user
eid = input("Enter an id to change the name of the employee: ")
sql.change_name(eid)
sql.printid_emp(eid)


# QUESTION 2
# c. Print the details of all the employees in a particular department (Department_id is input by the user)
did = input("Enter a department id: ")
sql.print_allin_dept(did)