import sqlite3
from cars import Car

working = True
# Creating a Connection object that represents the database
conn = sqlite3.connect(':memory:')
# To have the ability to access the individual items of a row by position or keyword value
conn.row_factory = sqlite3.Row
# Creating a Cursor object
c = conn.cursor()
# Call execute() method of cur to perform SQL commands
c.execute('''CREATE TABLE car_details
               (car_name text, owner_name text)''')

def print_commands():
    print("-----------------------------")
    print("You can: \n1. Create a new car entry \n2. View all car entries \n3. Exit")    
    print("-----------------------------")

while working:
    print_commands()
    choice = int(input("Enter your choice(1-3): "))
    if choice == 1:
        #create a new car entry
        sample = Car()
        print(f"Created {sample.car_name} owned by {sample.owner_name}")
        c.execute("INSERT INTO car_details VALUES(:carname, :ownername)", {'carname': sample.car_name, 'ownername': sample.owner_name})
    elif choice == 2:
        #View all car entries
        print("CAR DETAILS")
        c.execute("SELECT * FROM car_details")
        results = c.fetchall()
        for record in results:
            print(f"Car name- {record['car_name']}, Owner name- {record['owner_name']}")
    elif choice == 3:
        working = False
    else:
        pass

# Save (commit) the changes
conn.commit()
# Closing the connection
conn.close()