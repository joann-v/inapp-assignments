import sql_manage as ct

working = True
# Creating hospital table and populating it with values
ct.create_hospital()
ct.insert_hospitals()

# Creating doctor table and populating it with values
ct.create_doctor()
ct.insert_doctors()

#Displaying
#ct.display_hospitals()
#ct.display_doctors()

def print_commands():
    print("-----------------------------")
    print("You can: \n1. Get the list of doctors as per the user inputted specialty and salary \n2. Fetch all the doctors as per the given Hospital Id \n3. Display the hospital name of a doctor. \n4. Exit")    
    print("-----------------------------")

while working:
    print_commands()
    choice = int(input("Enter your choice(1-4): "))
    if choice == 1:
        # Get the list of doctors as per the user inputted specialty and salary 
        speciality = input("Enter the speciality: ")
        salary = int(input("Enter the salary: "))
        ct.find_doc(speciality.capitalize(), salary)
    elif choice == 2:
        # Fetch all the doctors as per the given Hospital Id
        hosp_id = input("Enter the hospital id: ")
        ct.find_docs_in_hosp(hosp_id)
    elif choice == 3:
        # Display the hospital name of a doctor
        doc_name = input("Enter the name of the doctor: ")
        ct.find_hosp(doc_name.capitalize())
    elif choice == 4:
        working = False
    else:
        pass