import sqlite3

conn = sqlite3.connect(':memory:')
conn.row_factory = sqlite3.Row
c = conn.cursor()

def create_hospital():
    c.execute('''CREATE TABLE hospital
               (hospital_id text primary key, 
                hospital_name text, 
                bed_count int)''')

hosp_list = [
    ('1', 'Mayo Clinic', 200),
    ('2', 'Cleveland Clinic', 400),
    ('3', 'Johns Hopkins', 1000),
    ('4', 'UCLA Medical Center', 1500)
]

def insert_hospitals():
    c.executemany("INSERT INTO hospital VALUES (?, ?, ?)", hosp_list)

def display_hospitals():
    print("\nHOSPITALS")
    c.execute("SELECT * FROM hospital")
    results = c.fetchall()
    for record in results:
        print(f"Hospital ID- {record['hospital_id']}, Hospital name- {record['hospital_name']}, Beds available- {record['bed_count']}")

def create_doctor():
    c.execute('''CREATE TABLE doctor
               (doctor_id text primary key, 
                doctor_name text, 
                hospital_id text,
                joining_date text,
                speciality text,
                salary int,
                experience text DEFAULT NULL,
                FOREIGN KEY(hospital_id) REFERENCES hospital(hospital_id))''')

doc_list = [
    ('101', 'David', '1', '2005-02-10', 'Pediatric', 40000, None),
    ('102', 'Michael', '1', '2018-07-23', 'Oncologist', 20000, None),
    ('103', 'Susan', '2', '2016-05-19', 'Gynecologist', 25000, None),
    ('104', 'Robert', '2', '2017-12-28', 'Pediatric', 28000, None),
    ('105', 'Linda', '3', '2004-06-04', 'Gynecologist', 42000, None),
    ('106', 'William', '3', '2012-09-11', 'Dermatologist', 30000, None),
    ('107', 'Richard', '4', '2014-08-21', 'Gynecologist', 32000, None),
    ('108', 'Karen', '4', '2011-10-17', 'Radiologist', 30000, None)
]

def insert_doctors():
    c.executemany("INSERT INTO doctor VALUES (?, ?, ?, ?, ?, ?, ?)", doc_list)

def display_doctors():
    print("\nDOCTORS")
    c.execute("SELECT * FROM doctor")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Doctor ID- {record['doctor_id']}, 
                  Doctor name- {record['doctor_name']}, 
                  Hospital ID- {record['hospital_id']},
                  Joining date- {record['joining_date']}, 
                  Speciality- {record['speciality']}, 
                  Salary- {record['salary']}, 
                  Experience- {record['experience']}
                  """)

def find_doc(speciality, salary):
    c.execute("SELECT * FROM doctor WHERE speciality = :sps AND salary >= :sal", {"sps": speciality, "sal": salary})
    results = c.fetchall()
    if len(results) == 0:
        print("No entries!")
    else:
        print(f"{speciality} doctors with salary above {salary} are: ")
    for record in results:
            print(f"{record['doctor_name']}, {record['speciality']}, {record['salary']}")

def find_docs_in_hosp(hospital_id):
    c.execute("SELECT * FROM doctor WHERE hospital_id = :hid", {"hid": hospital_id})
    results = c.fetchall()
    c.execute("SELECT hospital_name FROM hospital WHERE hospital_id = :hid", {"hid": hospital_id})
    hosp_results = c.fetchone()
    if len(results) == 0:
        print("No entries!")
    else:
        print(f"Doctors working at {hosp_results['hospital_name']} are: ")
    for record in results:
            print(f"{record['doctor_name']}, {record['speciality']}")

def find_hosp(doc_name):
    c.execute("SELECT * FROM doctor WHERE doctor_name = :dname", {"dname": doc_name})
    doc_results = c.fetchone()
    if len(doc_results) == 0:
        print("No entries!")
    else:
        c.execute("SELECT hospital_name FROM hospital WHERE hospital_id = :hid", {"hid": doc_results['hospital_id']})
        hosp = c.fetchone()
        print(f"{doc_results['doctor_name']} is working at {hosp['hospital_name']}.")