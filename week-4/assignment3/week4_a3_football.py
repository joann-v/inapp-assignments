import week4_a3_sql as sql

# QUESTION 1
# a. Analyse the tables in the database and draw the ER diagram.
# b. Print the names of both the Home Teams and Away Teams in each match played in 2015 and Full time Home Goals (FTHG) = 5
sql.print_q1b()
# c. Print the details of the matches where Arsenal is the Home Team and  Full Time Result (FTR) is “A” (Away Win)
sql.print_q1c()
# d. Print all the matches from the 2012 season till the 2015 season where Away Team is Bayern Munich and Full time Away Goals (FTAG) > 2
sql.print_q1d()
# e. Print all the matches where the Home Team name begins with “A” and Away Team name begins with “M”
sql.print_q1e()

# QUESTION 2
# a. Counts all the rows in the Teams table
sql.print_q2a()
# b. Print all the unique values that are included in the Season column in the Teams table
sql.print_q2b()
# c. Print the largest and smallest stadium capacity that is included in the Teams table
sql.print_q2c()
# d. Print the sum of squad players for all teams during the 2014 season from the Teams table [Answer - 1164]
sql.print_q2d()
# e. Query the Matches table to know how many goals did Man United score during home games on average? [Answer - 2.16]
sql.print_q2e()

# QUESTION 3
# a. Write a query that returns the HomeTeam, FTHG (number of home goals scored in a game) and 
#    FTAG (number of away goals scored in a game) from the Matches table. Only include data from the 2010 season and 
#    where ‘Aachen’ is the name of the home team. Return the results by the number of home goals scored in a game in descending order.
sql.print_q3a()
# b. Print the total number of home games each team won during the 2016 season in descending order of number of home games from the Matches table.
sql.print_q3b()
# c. Write a query that returns the first ten rows from the Unique_Teams table
sql.print_q3c()
# d. Print the Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables. 
#    Use the WHERE statement first and then use the JOIN statement to get the same result.
sql.print_q3d()
# e. Write a query that joins together the Unique_Teams data table and the Teams table, and returns the first 10 rows.
sql.print_q3e()
# f. Write a query that shows the Unique_Team_ID and TeamName from the Unique_Teams table and 
#    AvgAgeHome, Season and ForeignPlayersHome from the Teams table. Only return the first five rows.
sql.print_q3f()
# g. Write a query that shows the highest Match_ID for each team that ends in a “y” or a “r”. Along with the 
#    maximum Match_ID, display the Unique_Team_ID from the Teams_in_Matches table and the TeamName from the Unique_Teams table.
sql.print_q3g()