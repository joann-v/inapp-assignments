import sqlite3

conn = sqlite3.connect('database.sqlite')
conn.row_factory = sqlite3.Row
c = conn.cursor()

def print_allrecords():
    print("\nTEAMS")
    c.execute("SELECT DISTINCT TeamName FROM Teams")
    results = c.fetchall()
    for record in results:
        print(f"""{record['TeamName']}""")

def print_q1b():
    print("\nMATCHES PLAYED IN 2015 WITH FTHG = 5")
    c.execute("SELECT * FROM Matches WHERE Season = :season AND FTHG = :fthg", {'season' : 2015, 'fthg' : 5 })
    results = c.fetchall()
    for record in results:
        print(f"""
                  Home Team- {record['HomeTeam']}, Away Team- {record['AwayTeam']}, FTHG- {record['FTHG']}""")

def print_q1c():
    print("\nMATCHES WHERE ARSENAL IS HOME TEAM & FTR = A")
    c.execute("SELECT * FROM Matches WHERE HomeTeam = :ht AND FTR = :ftr", {'ht' : 'Arsenal', 'ftr' : 'A' })
    results = c.fetchall()
    for record in results:
        print(f"""
                  Div- {record['Div']}, Season- {record['Season']}, Date- {record['Date']}, Home Team- {record['HomeTeam']}, Away Team- {record['AwayTeam']}, FTR- {record['FTR']}""")

def print_q1d():
    print("\nMATCHES FROM 2012 TO 2015 WHERE BAYERN MUNICH IS AWAY TEAM & FTAG > 2")
    c.execute("SELECT * FROM Matches WHERE Season BETWEEN :from AND :to AND AwayTeam = :at AND FTAG > :ftag", {'at' : 'Bayern Munich', 'ftag' : 2, 'from': 2012, 'to': 2015 })
    results = c.fetchall()
    for record in results:
        print(f"""
                  Div- {record['Div']}, Season- {record['Season']}, Date- {record['Date']}, Home Team- {record['HomeTeam']}, Away Team- {record['AwayTeam']}, FTAG- {record['FTAG']}""")

def print_q1e():
    print("\nMATCHES WHERE HOME TEAM STARTS WITH A & AWAY TEAM WITH M")
    c.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%'")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Div- {record['Div']}, Season- {record['Season']}, Date- {record['Date']}, Home Team- {record['HomeTeam']}, Away Team- {record['AwayTeam']}""")

def print_q2a():
    print("\nCOUNT ROWS IN MATCH TABLE")
    c.execute("SELECT COUNT(*) FROM Teams")
    results = c.fetchone()
    print(results['count(*)'])

def print_q2b():
    print("\nSEASONS")
    c.execute("SELECT DISTINCT Season FROM Teams")
    results = c.fetchall()
    for record in results:
        print(f"""{record['Season']}""")

def print_q2c():
    print("\nSTADIUM CAPACITY")
    c.execute("SELECT MIN(StadiumCapacity) FROM Teams")
    results = c.fetchone()
    print(f"Min Capacity- {results['min(StadiumCapacity)']}")
    c.execute("SELECT MAX(StadiumCapacity) FROM Teams")
    results = c.fetchone()
    print(f"Max Capacity- {results['max(StadiumCapacity)']}")

def print_q2d():
    print("\nSUM OF PLAYERS FOR ALL TEAMS IN 2014")
    c.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season = :season", {'season' : 2014})
    results = c.fetchone()
    print(results['sum(KaderHome)'])

def print_q2e():
    print("\nAVG GOAL SCORE OF MAN UNITED DURING HOME GAMES")
    c.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam = :ht", {'ht' : 'Man United'})
    results = c.fetchone()
    print(results['avg(FTHG)'])

def print_q3a():
    print("\nMATCHES IN 2010 WHERE HOME TEAM IS AACHEN IN DESCENDING ORDER OF NUMBER OF HOME GOALS")
    c.execute("SELECT * FROM Matches WHERE Season = :season AND HomeTeam = :ht ORDER BY FTHG DESC", {'season' : 2010, 'ht' : 'Aachen' })
    results = c.fetchall()
    for record in results:
        print(f"""
                  Home Team- {record['HomeTeam']}, FTHG- {record['FTHG']}, FTAG- {record['FTAG']}""")

def print_q3b():
    print("\nTOTAL NUMBER OF GAMES WON BY EACH TEAM DURING 2016 IN DESCENDING ORDER OF NUMBER OF HOME GAMES")
    c.execute("SELECT COUNT(*), HomeTeam FROM Matches WHERE Season = :season AND FTR ='H' GROUP BY HomeTeam ORDER BY COUNT(*) DESC", {'season' : 2016})
    results = c.fetchall()
    for record in results:
        print(f"""
                  Home Team- {record['HomeTeam']}, Total Number of games won- {record['count(*)']}""")

def print_q3c():
    print("\nFIRST 10 ROWS OF UNIQUE_TEAMS")
    c.execute("SELECT * FROM Unique_Teams ORDER BY Unique_Team_ID LIMIT 10")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}""")

def print_q3d():
    print("\nPRINTING MATCH ID, TEAM ID & TEAM NAME- WHERE")
    c.execute("SELECT tm.Match_ID, tm.Unique_Team_ID, ut.Unique_Team_ID, ut.TeamName FROM Teams_in_Matches AS tm, Unique_Teams AS ut WHERE tm.Unique_Team_ID = ut.Unique_Team_ID ORDER BY tm.Match_ID LIMIT 20")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Match_ID- {record['Match_ID']}""")
    print("\nPRINTING MATCH ID, TEAM ID & TEAM NAME- JOIN")
    c.execute("SELECT tm.Match_ID, tm.Unique_Team_ID, ut.Unique_Team_ID, ut.TeamName FROM Teams_in_Matches AS tm INNER JOIN Unique_Teams AS ut ON tm.Unique_Team_ID = ut.Unique_Team_ID ORDER BY tm.Match_ID LIMIT 20")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Match_ID- {record['Match_ID']}""")

def print_q3e():
    print("\nJOIN UNIQUE_TEAMS & TEAMS TABLES- 1")
    c.execute("SELECT * FROM Unique_Teams AS ut INNER JOIN Teams AS t ON ut.TeamName = t.TeamName ORDER BY ut.Unique_Team_ID LIMIT 10")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Season- {record['Season']}""")

def print_q3f():
    print("\nJOIN UNIQUE_TEAMS & TEAMS TABLES- 2")
    c.execute("SELECT * FROM Unique_Teams AS ut INNER JOIN Teams AS t ON ut.TeamName = t.TeamName ORDER BY ut.Unique_Team_ID LIMIT 5")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, AvgAgeHome- {record['AvgAgeHome']}, Season- {record['Season']}, ForeignPlayersHome- {record['ForeignPlayersHome']}""")

def print_q3g():
    print("\nJOIN UNIQUE_TEAMS & TEAMS_IN_MATCHES TABLES")
    c.execute("SELECT MAX(tm.Match_ID), * FROM Unique_Teams AS ut INNER JOIN Teams_in_Matches AS tm ON ut.Unique_Team_ID = tm.Unique_Team_ID WHERE ut.TeamName LIKE '%y' OR ut.TeamName LIKE '%r' GROUP BY tm.Unique_Team_ID ORDER BY tm.Match_ID")
    results = c.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Highest Match_ID- {record['Match_ID']}""")