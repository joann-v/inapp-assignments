pets = {}

class Pet:
    #INITIALIZE ATTRIBUTES OF PET
    def __init__(self, species = None, name = ""):
        if species not in ['Dog', 'Cat', 'Horse', 'Hamster']:
            print("Species is not a dog, cat, horse or hamster!")
        self.species = species
        self.name = name
    #STR OF PET
    def __str__(self) -> str:
        if self.name == "":
            print(f"Species of: {self.species}, unnamed")
        else:
            print(f"Species of: {self.species}, named {self.name}")

class Dog(Pet):
    #INITIALIZE ATTRIBUTES OF DOG
    def __init__(self, name = "", chases = "Cats"):
        Pet.__init__(self, "Dog", name)
        self.chases = chases
    #STR OF DOG
    def __str__(self) -> str:
        if self.name == "":
            print(f"Species of: {self.species}, unnamed, chases {self.chases}")
        else:
            print(f"Species of: {self.species}, named {self.name}, chases {self.chases}")

class Cat(Pet):
    #INITIALIZE ATTRIBUTES OF CAT
    def __init__(self, name = "", hates = "Dogs"):
        Pet.__init__(self, "Cat", name)
        self.hates = hates
    #STR OF CAT
    def __str__(self) -> str:
        if self.name == "":
            print(f"Species of: {self.species}, unnamed, hates {self.hates}")
        else:
            print(f"Species of: {self.species}, named {self.name}, hates {self.hates}")

pets["Dogs"] = []
#Create dog
def createDog():
    name = input("Enter name of the dog: ")
    d= Dog(name)
    pets["Dogs"].append(d)

pets["Cats"] = []
#Create cat
def createCat():
    name = input("Enter name of the cat: ")
    c= Cat(name)
    pets["Cats"].append(c)

#Print dog
def printDog():
    for d in pets["Dogs"]:
        d.__str__()
        
#Print cat
def printCat():
    for c in pets["Cats"]:
        c.__str__()

#MAIN()
def main():
    print("CREATING 5 DOGS & 3 CATS!")
    for i in range(0, 5):
        print(f"Dog {i+1}")
        createDog()
    for i in range(0, 3):
        print(f"Cat {i+1}")
        createCat()
    printDog()
    printCat()
    
main()   